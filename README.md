KINBIOTICS Pipeline Dashboard
=============================

The KINBIOTICS pipeline dashboard visualises the results of the [edge](https://gitlab.ub.uni-bielefeld.de/kinbiotics-pipeline/edge-workflow) and [cloud](https://gitlab.ub.uni-bielefeld.de/kinbiotics-pipeline/cloud-workflow) workflows. It is run independently and just needs to be configured for [database](https://gitlab.ub.uni-bielefeld.de/kinbiotics-pipeline/database-api) access, by placing two text files `db_user` and `db_password` in this directory, containing the database user and password respectively.

It is the startet through [docker compose](https://docs.docker.com/compose/).
